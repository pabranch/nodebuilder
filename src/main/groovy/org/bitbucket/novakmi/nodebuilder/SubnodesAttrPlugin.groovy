//This is free software licensed under MIT License, see LICENSE file
//(https://bitbucket.org/novakmi/nodebuilder/src/LICENSE)

package org.bitbucket.novakmi.nodebuilder

/**
 This plugin supports expansion of node attribute subnodes to subnode nodes.
 e.g. mynode('val', subnodes: [<list of subnodes>])

 subnode list elements may contain:
 string  (String type)-  create subnode of name equal to the string, with null value
 [<string>, <val>] (List type)  - create subnode of name equal to string and value equal to val
 BuilderNode - add this node as subnode
 Example:
 mynode('val', subnodes: ['down', ['size', 30], new BuilderNode(name: 'length', value: '17m')])
 */
class SubnodesAttrPlugin extends NodeBuilderPlugin {

        protected PluginResult processNodeBefore(BuilderNode node, Object opaque, Map pluginMap) throws BuilderException {
                PluginResult retVal = PluginResult.NOT_ACCEPTED

                def subnodes = node.attributes['subnodes']
                if (subnodes != null) {
                        if (!(subnodes instanceof List)) {
                                throw new BuilderException("'subnode' attribute of node: ${node.name} path: ${BuilderNode.getNodePath(node)} has to be List type!")
                        }
                        subnodes.each { s ->
                                def snode = null;
                                switch (s) {
                                        case String:
                                                snode = new BuilderNode(name: s)
                                                break;
                                        case List:
                                                if (s.size() != 2) {
                                                        throw new BuilderException("node: ${node.name} path: ${BuilderNode.getNodePath(node)} 'subnode' element '${s}' List has to have exactly 2 elements [<name, val>]!")
                                                }
                                                if (!(s[0] instanceof String)) {
                                                        throw new BuilderException("node: ${node.name} path: ${BuilderNode.getNodePath(node)} 'subnode' element '${s}' List - first element (name) has to be String [<name, val>]!")
                                                }
                                                snode = new BuilderNode(name: s[0], value: s[1])
                                                break;
                                        case BuilderNode:
                                                snode = s
                                                break;
                                        default:
                                                throw new BuilderException("node: ${node.name} path: ${BuilderNode.getNodePath(node)} 'subnode' element '${s}' has to be String, List or BuilderNode!")

                                }
                                node.children += snode
                                retVal = PluginResult.PROCESSED_CONTINUE
                        }
                }

                return retVal
        }
}
