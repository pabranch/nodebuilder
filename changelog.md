#Nodebuilder change log

* 1.0.0.1 2016-12-04
   * fully compatible with 1.0.0, compiled groovy 2.1.8 - to be used with groovy < 2.1.9

* 1.0.0 2016-11-27

   * added `leftShift` operator to build from closure
   * updated `README.md` with description how to use `leftShift` operator
   * updated README.md` with description how to handle conflicts with builder native methods 

* 0.9.1 2015-06-07 

   * added helper functions to `TextPluginTreeNodeBuilder` (`getQuotes` and `trimAndQuoteLines`)
   * updated dependencies

* 0.9.0 2015-02-16  (0.9.0.1 - compile with groovy 2.1.8 - to be used with groovy < 2.1.9) 
    * updated dependencies
    * removed incorrect Override annotations (`TextPluginTreeNodeBuilder`)
    * `convertAlias` method made `public` (`TextNodeBuilder`)
    * added `getAliases` method (`TextNodeBuilder`)
    * plugin knows the last registered builder (`PluginTreeNodeBuilder`)
    * added `shallowCopy` method to the `BuilderNode`

* 0.8.0 2013-03-08
    * changed repository to https://bitbucket.org/novakmi/nodebuilder
    * added `getRootNode` and `getNodeText`
    * updated dependencies

* 0.7.0 2013-01-01
    *  updated dependencies to groovy 2.0.6 (testng 6.8, logback 1.0.9, slf4j 1.7.2)
    *  added SubnodeAttrPlugin + tests
    *  added LICENSE file and updated license info in source files
    *  changed groovy dependency to 2.0.0+

* 0.6.0  2012-07-14
    * `changelog.txt` renamed to `changelog.md` (Markdown is used for changelog)
    * dependencies updated to groovy 2.0.0

* 0.5.0 2012-04-29
    * TreeNodeBuilder
        * added support to declare alias keyword (alternative workaround for surrounding by quotes fro nodes clashing with
        groovy syntax (keyword, nodes with operators in name, etc.)

* 0.4.0 2012-04-01
    * TextPluginNodeBuilder
        * indent level reset to 0 in `reset()` method
        * `getBuiltText()` changed to `getText()`, `getText()` changed to `getTextBuffer()`
    * TreeNodeBuilder
        * added new interface method `findNode()`
    * Dependencies
        * logback-classic 1.0.0 -> 1.0.1
        * testng 6.2.1 -> 6.4

* 0.3.0  2012-02-12
    * added README.md and changelog.txt
    * Changed interface:
        * Renamed `SimpleNode` -> `BuilderNode`, `*SimpleBuilder` -> `*TreeBuilder`
        * Failure is now reported with `BuilderException` (not by return value)
    * PluginTreeNodeBuilder
        * accepts list of plugins in constructor (plugin registration)
        * `addPlugin()` renamed to `registerPlugin()`
        * added `unregisterPlugin()` method
        * added `getNumberOfRegisteredPlugins()`
    * NodeBuilderPlugin
        * added support for `reset()` method
    * TextPluginTreeNodeBuilder
        * added method `resetText()`
    * added TestNG unit tests for
        * `TreeNodeBuilder`
        * `PluginTreeNodeBuilder`
        * `TextPluginTreeNodeBuilder`

* 0.2.0  2011-12-31
    * Added `TextPluginSimpleNodeBuilder` for builders with textual output.

* 0.1.0  2011-12-14
    * Updated interface

* 0.0.1  2011-12-12
    * Initial version (split from plantumlbuilder project so common builder parts can be reused in another projects)
    * Builders:
        * `SimpleNodeBuilder`
        * `PluginSimpleNodeBuilder`